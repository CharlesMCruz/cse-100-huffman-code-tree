/**
 * Huffman Code Tree
 * CSE 100
 * Charles Cruz
 */
#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include "HCTree.hpp"

using namespace std;

typedef unsigned char byte;

// Function Prototypes
int streamError(const char*);
void compress(ifstream&, ofstream&, const char*, const char*);
void displayCompressionRatio(ios::pos_type&, ios::pos_type&);

int main(int argc, char* argv[]) {
  // Verify argc
  if (argc != 3) {
    cerr << argv[0] <<" called with incorrect arguments.\n"
         << "Usage: " << argv[0] << " infile outfile\n";
    return -1;
  }

  ifstream in(argv[1], ios::binary);
  ofstream out(argv[2], ios::binary);

  if (!in) {
    return streamError(argv[1]);
  }

  if (!out) {
    return streamError(argv[2]);
  }

  compress(in, out, argv[1], argv[2]);

  return 0;
}

/**
 * Print open error
 */
int streamError(const char* name) {
  cerr << "Error opening \"" << name << "\"\n";
  return -1;
}

/**
 * Read the file for frequencies, build the Huffman Code Tree, and write the encoded header/bits
 */
void compress(ifstream& in, ofstream& out, const char* inName, const char* outName) {
  byte symbols = 255;
  ios::pos_type inSize, outSize;
  BitOutputStream bOut(out);
  vector<int> freqs(256, 0);
  HCTree tree;

  // Get the input filesize and count the all 8-bit permutations (i.e. symbols) in the file
  cerr << "Reading from file \"" << inName << "\"... ";
  for (byte b = (byte)in.get(); !in.eof(); b = (byte)in.get()) {
    ++freqs[b];
  }
  in.clear();
  in.seekg(0, ios::end);
  inSize = in.tellg();
  cerr << "done.\n";

  // If the file is empty, close the streams and return
  if (!inSize) {
    in.close();
    out.close();
    cerr << "Input file is empty. Empty file \"" << outName << "\" created.\n";
    return;
  }

  // Display number of symbols detected and the input file size
  for (short i = 0; i < freqs.size(); ++i) {
    if (freqs[i] > 0) {
      ++symbols;
    }
  }
  cerr << "Found " << ((int)symbols) + 1 << " unique symbols in input file of size " << inSize
       << " bytes.\n";

  // Build the Huffman code tree
  cerr << "Building Huffman code tree... ";
  tree.build(freqs);
  cerr << "done.\n";

  // Write the header and then the encoded input bytes
  cerr << "Writing to file \"" << outName << "\"... ";
  out.write((char *)&symbols, sizeof(symbols));
  out.write((char *)&inSize, sizeof(inSize));
  tree.encodeTree(out, bOut);
  in.seekg(0, ios::beg);
  for (byte b = (byte)in.get(); !in.eof(); b = (byte)in.get()) {
    tree.encode(b, bOut);
  }
  bOut.flush();
  cerr << "done.\n";

  // Display the output file size
  outSize = out.tellp();
  cerr << "Output file has size " << outSize << " bytes.\n";

  // Close the streams
  in.close();
  out.close();

  // Display stats
  displayCompressionRatio(inSize, outSize);
}

/**
 * Display compression ratio
 */
void displayCompressionRatio(ios::pos_type& in, ios::pos_type& out) {
  cerr << "Compression ratio: " << (double)out / in << endl;
}
