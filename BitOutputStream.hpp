/**
 * Huffman Code Tree
 * CSE 100
 * Charles Cruz
 */
#ifndef BITOUTPUTSTREAM_HPP
#define BITOUTPUTSTREAM_HPP

#include <fstream>

typedef unsigned char byte;
typedef bool bit;

class BitOutputStream {
public:
  const static char BYTE_MAX = 8;
  BitOutputStream(std::ofstream &out) : out(out), numBits(0), buffer(0) {};
  void write(bit);
  void flush();
private:
  byte numBits;
  byte buffer;
  std::ofstream &out;
};

#endif // BITOUTPUTSTREAM_HPP
