/**
 * Huffman Code Tree
 * CSE 100
 * Charles Cruz
 */
#include "HCTree.hpp"
#include <iostream>

/**
 * ~HCTree
 * Delete root (which will recursively delete the child subtrees using the HCNode destructor)
 */
HCTree::~HCTree() {
  delete root;
}

/**
 * Build the Huffman code tree.
 */
void HCTree::build(const vector<int>& freqs) {
  priority_queue<HCNode*, vector<HCNode*>, HCNodePtrComp> pq;

  // Create leaves of the Huffman code tree from the freqs and put them on to the priority queue
  for (short i = 0; i < freqs.size(); ++i) {
    if (freqs[i] > 0) {
      HCNode* leaf = new HCNode(freqs[i], i);
      leaves[i] = leaf;
      pq.push(leaf);
    }
  }

  // From the priority queue, build the Huffman code tree
  while (pq.size() != 1) {
    // Pop two nodes and point to them
    HCNode* c0 = pq.top();
    pq.pop();
    HCNode* c1 = pq.top();
    pq.pop();

    // Create a new parent with the counts of both children
    HCNode* pNode = new HCNode(c0->count + c1->count, 0, c0, c1, 0);
    c0->p = c1->p = pNode;

    // Push that node onto the queue
    pq.push(pNode);
  }

  // Set the root node
  root = pq.top();
}

/**
 * Find and write the code based off of the symbol in the HCT
 */
void HCTree::encode(byte symbol, BitOutputStream& out) const {
  stack<bit> code;
  HCNode* parent;

  // Traverse through the HCT from leaf to root to find the proper encoding
  for (HCNode* pNode = leaves[symbol]; pNode != root; pNode = parent) {
    parent = pNode->p;
    code.push(pNode == parent->c0 ? 0 : 1);
  }

  // Write the code in the proper ordering
  for (; !code.empty(); code.pop()) {
    out.write(code.top());
  }
}

/**
 * Read from the bit stream and return the corresponding symbol
 */
byte HCTree::decode(BitInputStream& in) const {
  HCNode* pNode = root;

  // Follow down the tree to the child with the symbol
  while (pNode->c0 != 0 || pNode->c1 != 0) {
    pNode = in.read() ? pNode->c1 : pNode->c0;
  }

  return pNode->symbol;
}

/**
 * Write the encoded structure of the Huffman code tree.
 */
void HCTree::encodeTree(ofstream& out, BitOutputStream& bOut) const {
  stack<HCNode*> nodes;
  queue<bit> codes;
  HCNode* pNode;

  // Do a pre-order traversal to get the bit structure
  nodes.push(root);
  while (!nodes.empty()) {
    pNode = nodes.top();
    nodes.pop();

    // Push bit based off of parent/child relation
    if (pNode->p != 0) {
      codes.push(pNode == pNode->p->c0 ? 0 : 1);
    }

    // Write symbol if at a leaf node
    if (pNode->c0 == 0 && pNode->c1 == 0) {
      out.put(pNode->symbol);
    }

    if (pNode->c1 != 0) {
      nodes.push(pNode->c1);
    }

    if (pNode->c0 != 0) {
      nodes.push(pNode->c0);
    }
  }

  // Write codes
  for (; !codes.empty(); codes.pop()) {
    bOut.write(codes.front());
  }
}

/**
 * Reconstruct the Huffman coding tree from the input stream.
 */
bool HCTree::decodeTree(byte symbols, ifstream& in, BitInputStream& bIn) {
  queue<byte> chars;
  queue<bit> codes;

  // Read the characters
  for (short i = 0; i < ((short)symbols) + 1; ++i) {
    chars.push(in.get());
  }

  // Read the structure
  stack<HCNode*> nodes;
  HCNode* pNode = root = new HCNode(0, 0);

  // Construct the Huffman Code Tree
  for (short i = 2 * ((short)symbols); i > 0; --i) {
    if (in.eof()) {
      return false;
    }
    bit edge = bIn.read();

    if (!edge) {
      nodes.push(pNode);
      pNode->c0 = new HCNode(0, 0);
      pNode = pNode->c0;
    } else {
      pNode->symbol = chars.front();
      chars.pop();
      leaves[pNode->symbol] = pNode;
      pNode = nodes.top();
      nodes.pop();
      pNode->c1 = new HCNode(0, 0);
      pNode = pNode->c1;
    }
  }

  // Set the last node
  pNode->symbol = chars.front();
  leaves[pNode->symbol] = pNode;

  return true;
}
