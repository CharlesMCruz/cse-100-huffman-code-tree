/**
 * Huffman Code Tree
 * CSE 100
 * Charles Cruz
 */
#ifndef BITINPUTSTREAM_HPP
#define BITINPUTSTREAM_HPP

#include <fstream>

typedef unsigned char byte;
typedef bool bit;

class BitInputStream {
public:
  const static char BYTE_MAX = 8;
  BitInputStream(std::ifstream &in) : in(in), numBits(BYTE_MAX), buffer(0) {};
  bit read();
private:
  byte numBits;
  byte buffer;
  std::ifstream &in;
};

#endif // BITINPUTSTREAM_HPP
