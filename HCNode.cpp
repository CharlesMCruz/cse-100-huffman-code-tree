/**
 * Huffman Code Tree
 * CSE 100
 * Charles Cruz
 */
#include "HCNode.hpp"

/**
 * Delete child subtrees (implicitly recursive)
 */
HCNode::~HCNode() {
  if (c0 != 0) {
    delete c0;
  }
  if (c1 != 0) {
    delete c1;
  }
}

/**
 * operator< overload
 * @return: true if count of this object is greater than other
 */
bool HCNode::operator<(const HCNode& other) {
  return other.count < count;
}
