/**
 * Huffman Code Tree
 * CSE 100
 * Charles Cruz
 */
#include "BitOutputStream.hpp"

using namespace std;

/**
 * Add a bit (0 or 1) to buffer and write to the outstream when necessary.
 */
void BitOutputStream::write(bit b) {
  // When the buffer is full, flush it
  if (numBits == BYTE_MAX) {
    out.put(buffer);
    numBits = buffer = 0;
  }

  // Set the bit at position numBits from the right of the byte if necessary
  if (b) {
    buffer = buffer | (1 << numBits);
  }

  ++numBits;
}

/**
 * Write the buffer to outstream.
 */
void BitOutputStream::flush() {
  if (numBits != 0) {
    out.put(buffer);
    out.flush();
  }
}
