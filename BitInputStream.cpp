/**
 * Huffman Code Tree
 * CSE 100
 * Charles Cruz
 */
#include "BitInputStream.hpp"

/**
 * Read a bit from the buffer and return a bit
 */
bit BitInputStream::read() {
  // If all the bits have been read from the buffer, get the next character
  if(numBits == BYTE_MAX) {
    buffer = in.get();
    numBits = 0;
  }

  // Check whether the bit is set at position numBits from the right of the byte
  return buffer & (1 << numBits++) ? 1 : 0;
}
