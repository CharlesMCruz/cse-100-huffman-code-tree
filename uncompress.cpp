/**
 * Huffman Code Tree
 * CSE 100
 * Charles Cruz
 */
#include <iostream>
#include <fstream>
#include <vector>
#include "HCTree.hpp"

using namespace std;

typedef unsigned char byte;

// Function Prototypes
int streamError(const char*);
int eofError();
int decompress(ifstream&, ofstream&, const char*, const char*);
void displayUncompressionRatio(ios::pos_type&, ios::pos_type&);

int main(int argc, char* argv[]) {
  // Verify argc
  if (argc != 3) {
    cerr << argv[0] <<" called with incorrect arguments.\n"
         << "Usage: " << argv[0] << " infile outfile\n";
    return -1;
  }

  ifstream in(argv[1], ios::binary);
  ofstream out(argv[2], ios::binary);

  if (!in) {
    return streamError(argv[1]);
  }

  if (!out) {
    return streamError(argv[2]);
  }

  return decompress(in, out, argv[1], argv[2]);
}

/**
 * Print open error
 */
int streamError(const char* name) {
  cerr << "Error opening \"" << name << "\"\n";
  return -1;
}

/**
 * Print eof error
 */
int eofError() {
  cerr << "Problem: EOF while reading header.\n";
  return -1;
}

/**
 * Read the file header to rebuild the Huffman Code Tree
 */
int decompress(ifstream& in, ofstream& out, const char* inName, const char* outName) {
  byte symbols;
  ios::pos_type inSize, outSize;
  BitInputStream bIn(in);
  HCTree tree;

  // Get input file size
  in.clear();
  in.seekg(0, ios::end);
  inSize = in.tellg();
  in.seekg(0, ios::beg);

  // If the file is empty, close the streams and return
  if (!inSize) {
    in.close();
    out.close();
    cerr << "Input file is empty. Empty file \"" << outName << "\" created.\n";
    return 0;
  }

  // Read the header and build huffman code tree
  cerr << "Reading header from file \"" << inName << "\" and building Huffman code tree... ";
  in.read((char*)&symbols, sizeof(byte));
  in.read((char*)&outSize, sizeof(ios::pos_type));
  if (in.eof() || !tree.decodeTree(symbols, in, bIn)) {
    return eofError();
  }
  cerr << "done.\n";

  // Display number of symbols detected and the input file size
  cerr << "Uncompressed file will have " << ((int)symbols) + 1 << " unique symbols and size "
       << outSize << " bytes.\n";

  // Write to the output file
  cerr << "Writing to file \"" << outName <<"\"...";
  for (long double i = 0; i < outSize; ++i) {
    out.put(tree.decode(bIn));
  }
  cerr << "done.\n";

  // Close the streams
  in.close();
  out.close();

  // Display stats
  displayUncompressionRatio(inSize, outSize);

  return 0;
}

/**
 * Display compression ratio
 */
void displayUncompressionRatio(ios::pos_type& in, ios::pos_type& out) {
  cerr << "Uncompression ratio: " << (double)out / in << endl;
}
